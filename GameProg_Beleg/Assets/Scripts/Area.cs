﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour {

    public Transform FishPrefab;
    public Transform EnemyPrefab;
    public List<Fish> Fishes;
    public List<Enemy> Enemies;

    [Header("Spawn Settings")]
    public int NumberOfFishes;
    public int NumberOfEnemies;
    public float Bounds;
    public float SpawnRadius;

    // Use this for initialization
    void Start () {
        Fishes = new List<Fish>();
        Enemies = new List<Enemy>();
        Spawn(FishPrefab, NumberOfFishes);
        Spawn(EnemyPrefab, NumberOfEnemies);

        Fishes.AddRange(FindObjectsOfType<Fish>());
        Enemies.AddRange(FindObjectsOfType<Enemy>());
	}

    void Spawn(Transform prefab, int count)
    {
        for (int i = 0; i < count; i++)
        {
            // Spawn prefab within a radius
            Instantiate(prefab, new Vector3(Random.Range(-SpawnRadius, SpawnRadius), Random.Range(-SpawnRadius, SpawnRadius), 0), Quaternion.identity);
        }
    }

    public List<Fish> GetNeighbors(Fish fish, float radius)
    {
        List<Fish> neighborsFound = new List<Fish>();

        foreach (var otherFish in Fishes)
        {
            if (otherFish == fish)
            {
                continue;
            }

            if (Vector3.Distance(fish.Position, otherFish.Position) <= radius)
            {
                neighborsFound.Add(otherFish);
            }
        }

        return neighborsFound;
    }

    public List<Enemy> GetEnemies(Fish fish, float radius)
    {
        List<Enemy> returnEnemies = new List<Enemy>();

        foreach (var enemy in Enemies)
        {
            if (Vector3.Distance(fish.Position, enemy.Position) <= radius)
            {
                returnEnemies.Add(enemy);
            }
        }

        return returnEnemies;
    }
}