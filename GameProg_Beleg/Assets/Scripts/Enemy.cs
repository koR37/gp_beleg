﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Fish {

    public float AggroRadius = 0.04f;

    protected override Vector3 Combine()
    {
        return FishConfig.WanderPriority * Wander();
    }

    bool PlayerInRange(float radius)
    {
        return true;
    }
}