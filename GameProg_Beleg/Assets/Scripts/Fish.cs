﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour {

    public Vector3 Position;
    public Vector3 Velocity;
    public Vector3 Acceleration;

    public Area Area;
    public FishConfig FishConfig;

    public Vector3 WanderTarget;

    // Use this for initialization
    void Start () {
        Area = FindObjectOfType<Area>();
        FishConfig = FindObjectOfType<FishConfig>();

        Position = transform.position;
        Velocity = new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0);
    }

    private void Update()
    {
        Acceleration = Combine();
        Acceleration = Vector3.ClampMagnitude(Acceleration, FishConfig.MaxAccel);
        Velocity = Velocity + Acceleration * Time.deltaTime;
        Velocity = Vector3.ClampMagnitude(Velocity, FishConfig.MaxVelocity);

        Position = Position + Velocity * Time.deltaTime;
        WrapAround(ref Position, -Area.Bounds, Area.Bounds);
        transform.position = Position;
    }

    virtual protected Vector3 Combine()
    {
        Vector3 finalVector = FishConfig.CohesionPriority * Cohesion()
                            + FishConfig.WanderPriority * Wander()
                            + FishConfig.AlignmentPriority * Alignment()
                            + FishConfig.SeperationPriority * Seperation()
                            + FishConfig.AvoidancePriority * Avoidance();
        return finalVector;
    }

    protected Vector3 Wander()
    {
        float jitter = FishConfig.WanderJitter * Time.deltaTime;
        WanderTarget += new Vector3(RandomBinomial() * jitter, RandomBinomial() * jitter, 0);
        WanderTarget = WanderTarget.normalized;
        WanderTarget *= FishConfig.WanderRadius;
        Vector3 targetInLocalSpace = WanderTarget + new Vector3(FishConfig.WanderDistance, FishConfig.WanderDistance, 0);
        Vector3 targetInWorldSpace = transform.TransformPoint(targetInLocalSpace);
        targetInWorldSpace -= this.Position;
        return targetInWorldSpace.normalized;
    }

    Vector3 Cohesion()
    {
        Vector3 cohesionVector = new Vector3();
        int countFish = 0;

        var neighbors = Area.GetNeighbors(this, FishConfig.CohesionRadius);

        if (neighbors.Count == 0)
        {
            return cohesionVector;
        }

        foreach (var fish in neighbors)
        {
            if (IsInFOV(fish.Position))
            {
                cohesionVector += fish.Position;
                countFish++;
            }
        }

        if (countFish == 0)
        {
            return cohesionVector;
        }

        cohesionVector /= countFish;
        cohesionVector = cohesionVector - this.Position;
        cohesionVector = Vector3.Normalize(cohesionVector);

        return cohesionVector;
    }

    Vector3 Alignment()
    {
        Vector3 alignVector = new Vector3();
        var fishes = Area.GetNeighbors(this, FishConfig.AlignmentRadius);

        if (fishes.Count == 0)
        {
            return alignVector;
        }

        foreach (var fish in fishes)
        {
            if (IsInFOV(fish.Position))
            {
                alignVector += fish.Velocity;
            }
        }

        return alignVector.normalized;
    }

    Vector3 Seperation()
    {
        Vector3 seperateVector = new Vector3();
        var fishes = Area.GetNeighbors(this, FishConfig.SeperationRadius);

        if (fishes.Count == 0)
        {
            return seperateVector;
        }

        foreach (var fish in fishes)
        {
            if (IsInFOV(fish.Position))
            {
                Vector3 movingTowards = this.Position - fish.Position;
                if (movingTowards.magnitude > 0)
                {
                    seperateVector += movingTowards.normalized / movingTowards.magnitude;
                }
            }
        }

        return seperateVector;
    }

    Vector3 Avoidance()
    {
        Vector3 avoidVector = new Vector3();
        var enemyList = Area.GetEnemies(this, FishConfig.AvoidanceRadius);

        if (enemyList.Count == 0)
        {
            return avoidVector;
        }

        foreach (var enemy in enemyList)
        {
            avoidVector += RunAway(enemy.Position);
        }

        return avoidVector.normalized;
    }

    Vector3 RunAway(Vector3 target)
    {
        Vector3 neededVelocity = (Position - target).normalized * FishConfig.MaxVelocity;
        return neededVelocity - Velocity;
    }

    bool IsInFOV(Vector3 vector)
    {
        return Vector3.Angle(this.Velocity, vector - this.Position) <= FishConfig.MaxFOV;
    }

    void WrapAround(ref Vector3 vector, float min, float max)
    {
        vector.x = WrapAroundFloat(vector.x, min, max);
        vector.y = WrapAroundFloat(vector.y, min, max);
        vector.z = WrapAroundFloat(vector.z, min, max);
    }

    float WrapAroundFloat(float value, float min, float max)
    {
        if (value > max)
        {
            value = min;
        }
        else if (value < min)
        {
            value = max;
        }
        return value;
    }

    float RandomBinomial()
    {
        return Random.Range(0f, 1f) - Random.Range(0f, 1f);
    }
}