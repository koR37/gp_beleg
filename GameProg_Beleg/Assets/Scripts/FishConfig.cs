﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishConfig : MonoBehaviour {

    [Header("Fish Settings")]
    public float MaxFOV = 180;
    public float MaxAccel;
    public float MaxVelocity;

    [Header("Wander Settings")]
    public float WanderJitter;
    public float WanderRadius;
    public float WanderDistance;
    public float WanderPriority;

    [Header("Behaviour Settings")]
    public float CohesionRadius;
    public float CohesionPriority;

    public float AlignmentRadius;
    public float AlignmentPriority;

    public float SeperationRadius;
    public float SeperationPriority;

    public float AvoidanceRadius;
    public float AvoidancePriority;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
